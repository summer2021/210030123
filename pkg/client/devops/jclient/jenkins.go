package jclient

import (
	"fmt"
	"time"

	appCfg "github.com/wxharry/jenkins-cli/app/config"
	"github.com/wxharry/jenkins-cli/client"
	"kubesphere.io/devops/pkg/client/devops/jenkins"
)

type JenkinsClient struct {
	// client.JenkinsCore
}

type JenkinsOptions struct {
	Host            string        `json:",omitempty" yaml:"host" description:"Jenkins service host address"`
	Username        string        `json:",omitempty" yaml:"username" description:"Jenkins admin username"`
	Password        string        `json:",omitempty" yaml:"password" description:"Jenkins admin password"`
	MaxConnections  int           `json:"maxConnections,omitempty" yaml:"maxConnections" description:"Maximum connections allowed to connect to Jenkins"`
	Namespace       string        `json:"namespace,omitempty" yaml:"namespace"`
	WorkerNamespace string        `json:"workerNamespace,omitempty" yaml:"workerNamespace"`
	ReloadCasCDelay time.Duration `json:"reloadCasCDelay,omitempty" yaml:"reloadCasCDelay"`
}

var rootJenkinsOptions JenkinsOptions

// newJenkinsServer
func NewJenkinsRootOptions(options *jenkins.Options) error {
	if options.Host == "" {
		return fmt.Errorf("cannot get jenkins host")
	}
	rootJenkinsOptions.Host = options.Host
	rootJenkinsOptions.Username = options.Username
	rootJenkinsOptions.Password = options.Password
	return nil
}

// GetJcliRootOptions returns the rootJenkinsOptions
func GetJcliRootOptions() *JenkinsOptions {
	return &rootJenkinsOptions
}

// getCurrentJcli gets the current jclient jenkinscore and returns the jenkins server
func getCurrentJcli(jcli *client.JenkinsCore) (jenkins *appCfg.JenkinsServer) {
	// jenkins = getCurrentJenkinsServerFromOptions()
	jcli.URL = rootJenkinsOptions.Host
	jcli.UserName = rootJenkinsOptions.Username
	jcli.Token = rootJenkinsOptions.Password
	// jcli.Proxy = jenkins.Proxy
	// jcli.ProxyAuth = jenkins.ProxyAuth
	// jcli.InsecureSkipVerify = jenkins.InsecureSkipVerify
	return
}

// getCurrentJenkinsServerFromOptions returns current jenkins server from rootJenkinsOptions
func getCurrentJenkinsServerFromOptions() (server *appCfg.JenkinsServer) {
	opt := rootJenkinsOptions
	server.URL = opt.Host
	server.UserName = opt.Username
	server.Token = opt.Password
	return
}
