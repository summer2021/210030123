package jclient

import (
	"fmt"
	"net/http"

	"github.com/emicklei/go-restful"
	"github.com/wxharry/jenkins-cli/client"
	"k8s.io/klog"
	"kubesphere.io/devops/pkg/api/devops/v1alpha3"
	devopsv1alpha3 "kubesphere.io/devops/pkg/api/devops/v1alpha3"
	"kubesphere.io/devops/pkg/client/devops"
)

func (j *JenkinsClient) CreateProjectPipeline(projectId string, pipeline *v1alpha3.Pipeline) (string, error) {
	switch pipeline.Spec.Type {
	case devopsv1alpha3.NoScmPipelineType:
		createPayload, err := getCreatePayload(pipeline.Spec.Pipeline)
		if err != nil {
			return "", restful.NewError(http.StatusInternalServerError, err.Error())
		}
		jclient := client.JobClient{
			JenkinsCore: client.JenkinsCore{},
		}
		getCurrentJcli(&(jclient.JenkinsCore))
		projectPipelineName := fmt.Sprintf("%s %s", projectId, pipeline.Spec.Pipeline.Name)
		job, _ := jclient.GetJob(projectPipelineName)
		if job != nil {
			err := fmt.Errorf("job name [%s] has been used", job.Name)
			return "", restful.NewError(http.StatusConflict, err.Error())
		}
		err = jclient.CreateJobInFolder(*createPayload, projectId)
		if err != nil {
			return "", restful.NewError(devops.GetDevOpsStatusCode(err), err.Error())
		}

		return pipeline.Name, nil
	case devopsv1alpha3.MultiBranchPipelineType:
		createPayload, err := getCreateMultiBranchPipelinePayload(pipeline.Spec.Pipeline)
		if err != nil {
			return "", restful.NewError(http.StatusInternalServerError, err.Error())
		}
		jclient := client.JobClient{
			JenkinsCore: client.JenkinsCore{},
		}
		getCurrentJcli(&(jclient.JenkinsCore))
		projectPipelineName := fmt.Sprintf("%s %s", projectId, pipeline.Spec.Pipeline.Name)
		job, _ := jclient.GetJob(projectPipelineName)
		if job != nil {
			err := fmt.Errorf("job name [%s] has been used", job.Name)
			return "", restful.NewError(http.StatusConflict, err.Error())
		}
		err = jclient.CreateJobInFolder(*createPayload, projectId)
		if err != nil {
			return "", restful.NewError(devops.GetDevOpsStatusCode(err), err.Error())
		}
		return pipeline.Name, nil
	default:
		err := fmt.Errorf("error unsupport job type")
		klog.Errorf("%+v", err)
		return "", restful.NewError(http.StatusBadRequest, err.Error())
	}

}

func (j *JenkinsClient) DeleteProjectPipeline(projectId string, pipelineId string) (string, error) {

	return "", nil
}
func (j *JenkinsClient) UpdateProjectPipeline(projectId string, pipeline *devopsv1alpha3.Pipeline) (string, error) {
	return "", nil
}

func (j *JenkinsClient) GetProjectPipelineConfig(projectId, pipelineId string) (*devopsv1alpha3.Pipeline, error) {
	return nil, nil
}

func getCreatePayload(pipeline *devopsv1alpha3.NoScmPipeline) (jobPayload *client.CreateJobPayload, err error) {
	// NoScmPipeline do not have copy mode to create a pipeline
	jobPayload = &client.CreateJobPayload{
		Mode: "org.jenkinsci.plugins.workflow.job.WorkflowJob",
		From: "",
		Name: pipeline.Name,
	}
	return
}

func getCreateMultiBranchPipelinePayload(pipeline *devopsv1alpha3.NoScmPipeline) (jobPayload *client.CreateJobPayload, err error) {
	jobPayload = &client.CreateJobPayload{
		Mode: "org.jenkinsci.plugins.workflow.multibranch.WorkflowMultiBranchProject",
		From: "",
		Name: pipeline.Name,
	}
	return
}
func parsePipelineConfigJson(j *client.Job, p *client.Pipeline) (pipeline *devopsv1alpha3.NoScmPipeline, err error) {
	pipeline = &devopsv1alpha3.NoScmPipeline{
		Name:        j.Name,
		Description: "",
		Jenkinsfile: p.Script,
	}
	return
}
